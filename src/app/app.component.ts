import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'copiivspandemii';
  newPost = {
    photoUrl: '',
    description: '',
    facebookUrl: ''
  }

  posts = [];

  constructor(private router: Router,
    private http: HttpClient) { }

  ngOnInit() {
    this.http.get('assets/posts.json').subscribe((data: any[]) => {
      this.posts.push(...data);
    })
  }

  goToFacebook(url) {
    window.open(url, '_blank');
  }

  save() {
    const post = JSON.parse(JSON.stringify(this.newPost));
    this.posts.push(post);
    this.newPost = {
      photoUrl: '',
      description: '',
      facebookUrl: ''
    };
  }

}
